package models;
import play.db.ebean.Model;
import java.util.*;
import javax.persistence.*;

/**
 * Created by Administrator on 2/9/2015.
 */

@Entity
public class Warehouse extends Model {
	@Id
	public Long id;
    public String name;
    @OneToOne
    public Address address;
    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();

    public String toString() {
        return name;
    }
}
